from flask import Flask, jsonify, request
import os
import pandas as pd
from flask_cors import CORS
import datetime
import requests
import json
from google.cloud import bigquery
import pymongo
from geopy.geocoders import Nominatim
from geopy.distance import geodesic
from datetime import datetime, timedelta
from apscheduler.schedulers.background import BackgroundScheduler
from celery import Celery
import polyline
import math
import ast

app = Flask(__name__)
CORS(app)

app.config['CELERY_BROKER_URL'] = 'redis://red-cj7r3dc5kgrc73bkvuv0:6379'

celery = Celery(app.name)
celery.conf.broker_url = app.config['CELERY_BROKER_URL']
celery.conf.timezone = 'UTC'

mongo_uri = os.environ.get('mongoDB')
apikey = os.environ.get('apiKey')
weather_apikey = os.environ.get('openweather')
access_token = os.environ.get('mapbox')

# Get Google cloud Service Account Key
# We're doing it this way because 
# saving the key as environment variables skips some of the special characters

client = pymongo.MongoClient(mongo_uri)
db = client['TraffoozeDBS']
collection = db['google_account_key']

projection = {"_id": 0}

# Retrieve the document without the _id field
retrieved_document = collection.find_one({}, projection)

if retrieved_document:
    service_account_info = retrieved_document
else:
    service_account_info = {}
# Close the MongoDB client
client.close()

@app.route('/', methods=['GET'])
def hello_world():
    return "Hello Beautiful World"

@app.route('/keep_alive', methods=['GET'])
def keep_alive():
    return "OK", 200
    
@app.route('/metadata', methods=['GET'])
def get_metadata():

    mongo_url = mongo_uri
    client = pymongo.MongoClient(mongo_url)
    db = client['TraffoozeDBS']
    collection = db['roads_metadata']

    projection = {"_id": 0, "length":0, "shape":0, "coord_list": 0, "start_lat":0, "start_lng":0}

    roads_metadata = list(collection.find(projection=projection))

    client.close()
    
    if roads_metadata:
        return jsonify(roads_metadata)
    else:
        return 404

@app.route('/get_traffic_flow', methods=['POST'])
def get_traffic_flow_predictions():

    data = request.get_json()

    road_id = data["road_id"]
    timestamp = data["timestamp"]

    speed_list = []
    jam_factor_list = []
    timestamp_list = []

    client = bigquery.Client.from_service_account_info(service_account_info)

    query = f"""
        SELECT *
        FROM `disco-math-385807.traffooze.traffic_flow_predictions`
        WHERE road_id = {road_id} AND timestamp >= DATETIME('{timestamp}')
        ORDER BY timestamp ASC
        LIMIT 12
    """

    query_job = client.query(query)

    results = query_job.result()

    for row in results:
        timestamp_string = row[1].strftime("%Y-%m-%d %H:%M")
        timestamp_list.append(timestamp_string)
        
        rounded_speed = round(row[2], 2)  
        rounded_jam_factor = round(row[3], 2)
        
        speed_list.append(rounded_speed)  
        jam_factor_list.append(rounded_jam_factor)

    results = {"speed": speed_list, "jamFactor": jam_factor_list, "timestamp": timestamp_list}

    return jsonify(results)

@app.route('/get_traffic_count', methods=['POST'])
def get_traffic_count_predictions():

    data = request.get_json()

    camera_id = data["camera_id"]
    timestamp = data["timestamp"]

    count_list = []
    timestamp_list = []
    
    client = bigquery.Client.from_service_account_info(service_account_info)

    # query to get traffic count predictions
    query = f"""
        SELECT *
        FROM `disco-math-385807.traffooze.traffic_count_predictions`
        WHERE Camera_ID = {camera_id} AND timestamp >= DATETIME('{timestamp}')
        ORDER BY timestamp ASC
        LIMIT 12
    """

    query_job = client.query(query)

    results = query_job.result()

    for row in results:
        timestamp_string = row[1].strftime("%Y-%m-%d %H:%M")
        timestamp_list.append(timestamp_string)
        
        my_count = row[2]
        
        count_list.append(my_count)  

    results = {"count": count_list, "timestamp": timestamp_list}

    return jsonify(results)

def haversine_distance(coord1, coord2):
    lat1, lon1 = coord1
    lat2, lon2 = coord2

    R = 6371.0  # Earth's radius in kilometers

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)

    a = math.sin(dlat / 2)**2 + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.sin(dlon / 2)**2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    distance = R * c
    return distance

def find_closest_route(target_route, routes_df):
    closest_distance = float('inf')
    closest_route_index = None

    for idx, route_coords in enumerate(routes_df['coord_list']):
        total_distance = sum(haversine_distance(coord1, coord2) for coord1, coord2 in zip(target_route, route_coords))
        
        if total_distance < closest_distance:
            closest_distance = total_distance
            closest_route_index = idx

    return closest_route_index

# The Render free instance is not able to run this function
# without running out of memory
# since it only has 512MB of RAM
@app.route('/get_travel_time', methods=['POST'])
def get_travel_time():

    data = request.get_json()

    coordinates = data["coordinates"]
    timestamp = data["timestamp"]

    service_account_info = {
        "type": os.environ.get("type"),
        "project_id": os.environ.get("project_id"),
        "private_key_id": os.environ.get("private_key_id"),
        "private_key": os.environ.get("private_key"),
        "client_email": os.environ.get("client_email"),
        "client_id": os.environ.get("client_id"),
        "auth_uri": os.environ.get("auth_uri"),
        "token_uri": os.environ.get("token_uri"),
        "auth_provider_x509_cert_url": os.environ.get("auth_provider_x509_cert_url"),
        "client_x509_cert_url": os.environ.get("client_x509_cert_url"),
        "universe_domain": os.environ.get("universe_domain")
    }

    client = bigquery.Client.from_service_account_info(service_account_info)

    query = f"""
        SELECT road_id, jamFactor_prediction
        FROM `disco-math-385807.traffooze.traffic_flow_predictions`
        WHERE timestamp = DATETIME('{timestamp}')
    """

    query_job = client.query(query)

    pred_df = query_job.to_dataframe()

    mongo_url = mongo_uri
    client = pymongo.MongoClient(mongo_url)
    db = client['TraffoozeDBS']
    collection = db['roads_metadata']

    projection = {"_id": 0, "road_id": 1, "coord_list":1}

    cursor = collection.find(projection=projection)

    data_list = list(cursor)
    metadata_df = pd.DataFrame(data_list)

    client.close()

    pred_df = pred_df.merge(metadata_df, on='road_id', how='left')

    url = f'https://api.mapbox.com/directions/v5/mapbox/driving/{coordinates}?access_token={access_token}&alternatives=true'
    response = requests.get(url)
    data = response.json()

    routes = data["routes"]

    route_list = []

    for route in routes:
        distance = route["distance"]
        duration = route["duration"]

        decoded = polyline.decode(route["geometry"])
            
        closest_route_index = find_closest_route(decoded, pred_df)
        closest_jam_factor = pred_df.loc[closest_route_index, 'jamFactor_prediction']

        predict_duration = duration * (1 + closest_jam_factor / 10)
        
        expected_route = {
            "distance": distance,
            "normal_duration": duration,
            "predicted_duration": predict_duration,
            "jam_factor": closest_jam_factor,
            "coord_list": decoded
        }

        route_list.append(expected_route)

    return jsonify(route_list)

# Using celery to process long running tasks in the background
# We're not using this function because we're using free plan on Render
# and the free instance type only has 512MB of RAM
# and it will run out of memory when running a task this intensive

@celery.task
def traffic_flow_predictions():

    client = pymongo.MongoClient(mongo_uri)
    db = client['TraffoozeDBS']
    collection = db['test_predictions']
          
    current_datetime = datetime.now()

    data_to_insert = {'status': "running", 'time': current_datetime}
          
    collection.insert_one(data_to_insert)
    client.close()
    
    client = pymongo.MongoClient(mongo_uri)
    db = client['TraffoozeDBS']
    collection = db['roads_metadata']

    cursor = collection.find()

    data_list = list(cursor)
    metadata_df = pd.DataFrame(data_list)

    client.close()

    current_datetime = datetime.now()

    start_date = current_datetime.replace(hour=0, minute=0, second=0, microsecond=0)

    end_date = start_date + timedelta(hours=1)

    timestamps = [start_date + timedelta(minutes=i*5) for i in range(int((end_date - start_date).total_seconds() // 300))]

    prediction_data_list = []

    for index, row in metadata_df.iterrows():
        road_id = row["road_id"]
        road_length = row["length"]
        road_shape = row["shape"]
        road_start_lat = row['start_lat']
        road_start_lng = row['start_lng']
        prediction_data = pd.DataFrame({'road_id': [road_id] * len(timestamps),
                                        'length': [road_length] * len(timestamps),
                                        'shape': [road_shape] * len(timestamps),
                                        'start_lat': [road_start_lat] * len(timestamps),
                                        'start_lng': [road_start_lng] * len(timestamps),
                                        'timestamp': timestamps})
        prediction_data_list.append(prediction_data)

    combined_data = pd.concat(prediction_data_list, ignore_index=True)

    combined_data['timestamp'] = pd.to_datetime(combined_data['timestamp'])
    original_time_zone = 'Asia/Singapore'
    combined_data['timestamp'] = combined_data['timestamp'].dt.tz_localize(original_time_zone)
    combined_data['timestamp'] = combined_data['timestamp'].dt.tz_convert('GMT')

    locations = [
        {"latitude": 1.35806, "longitude": 103.940277},  # Tampines estate
        {"latitude": 1.36667, "longitude": 103.883331},  # Somapah Serangoon
        {"latitude": 1.36667, "longitude": 103.800003},  # Republic of Singapore
        {"latitude": 1.28967, "longitude": 103.850067},  # Singapore
        {"latitude": 1.41, "longitude": 103.874168},  # Seletar
        {"latitude": 1.37833, "longitude": 103.931938},  # Kampong Pasir Ris
        {"latitude": 1.42611, "longitude": 103.824173},  # Chye Kay
        {"latitude": 1.35, "longitude": 103.833328},  # Bright Hill Crescent
        {"latitude": 1.30139, "longitude": 103.797501},  # Tanglin Halt
        {"latitude": 1.44444, "longitude": 103.776672},  # Woodlands
        {"latitude": 1.35722, "longitude": 103.836388},  # Thomson Park
        {"latitude": 1.31139, "longitude": 103.797783},  # Chinese Gardens
        {"latitude": 1.35222, "longitude": 103.898064},  # Kampong Siren
        {"latitude": 1.36278, "longitude": 103.908333},  # Punggol Estate
    ]

    weather_data_dict = {}

    def get_weather_data(location, timestamp):
        latitude, longitude = location["latitude"], location["longitude"]
        api_url = f"https://api.openweathermap.org/data/2.5/forecast?lat={latitude}&lon={longitude}&appid={weather_apikey}"
        response = requests.get(api_url)
        weather_list = response.json()["list"]
        return location, weather_list
    
    for location in locations:
        location_data, weather_list = get_weather_data(location, timestamps[0])  # Get weather data for the first timestamp
        weather_data_dict[location_data['latitude'], location_data['longitude']] = weather_list

    def calculate_distance(coord1, coord2):
        return geodesic(coord1, coord2).meters
    
    def get_closest_weather_to_timestamp(weather, timestamp):
        target_timestamp = int(datetime.timestamp(timestamp))
        closest_weather = min(weather, key=lambda x: abs(x['dt'] - target_timestamp))
        return closest_weather

    def get_weather_attributes(location, timestamp):
        weather_list = weather_data_dict[location['latitude'], location['longitude']]
        closest_weather = get_closest_weather_to_timestamp(weather_list, timestamp)
        
        return {
            'temperature': closest_weather.get('main', {}).get('temp', 0),
            'humidity': closest_weather.get('main', {}).get('humidity', 0),
            'pressure': closest_weather.get('main', {}).get('pressure', 0),
            'visibility': closest_weather.get('visibility', 0),
            'wind_speed': closest_weather.get('wind', {}).get('speed', 0),
            'wind_degree': closest_weather.get('wind', {}).get('deg', 0),
            'wind_gust': closest_weather.get('wind', {}).get('gust', 0),
            'clouds': closest_weather.get('clouds', {}).get('all', 0),
            'rain_3h': closest_weather.get('rain', {}).get('3h', 0)
        }
    
    combined_data = pd.concat([combined_data, combined_data.apply(lambda row: get_weather_attributes(min(locations, key=lambda loc: calculate_distance((row['start_lat'], row['start_lng']), (loc['latitude'], loc['longitude']))), row['timestamp']), axis=1, result_type='expand')], axis=1)
    
    results = combined_data.to_dict("records")

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['traffic_flow_predictions']

    collection.insert_many(results)

    client.close()
    
    return jsonify(results)

@app.route('/save_trafficjam', methods=['GET'])
def save_trafficjam():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['jvs_sample_trafficjam']

    headers = { 'AccountKey' : apikey,
             'accept' : 'application/json'}

    response = requests.get('http://datamall2.mytransport.sg/ltaodataservice/TrafficIncidents', headers=headers)
    data = response.json().get("value", [])  # Use .get() method to get the value or return an empty list if it doesn't exist

    if not data:
        client.close()
        return "No traffic incidents data available from the API."
    
    df = pd.DataFrame(data)
    df[['Date', 'Time']] = df['Message'].str.extract(r'\((.*?)\)(.*?) ')

    df['Message'] = df['Message'].str.replace(r'\(\d+/\d+\)\d+:\d+ ', '', regex=True)

    current_year = pd.Timestamp.now().year
    df['Date'] = df['Date'] + '/' + str(current_year)

    jam = df.loc[df['Type'] == "Heavy Traffic"]

    traffic_jams = []

    geolocator = Nominatim(user_agent="myGeocoder")

    for index, row in jam.iterrows():
        date = row["Date"]
        time = row["Time"]
        message = row['Message']
        location = str(row["Latitude"]) + "," + str(row["Longitude"])
        try:
            location_info = geolocator.reverse(location, exactly_one=True)
            address = location_info.address
        except Exception as e:
            address = None

        if address is not None:
            traffic_jam = {}

            traffic_jam["date"] = date
            traffic_jam["time"] = time
            traffic_jam["message"] = message
            traffic_jam["location"] = location
            traffic_jam["address"] = address

            traffic_jams.append(traffic_jam)   

    #result = collection.insert_many(traffic_jams)

    # Perform condition checking for duplicates

    existing_records = collection.find({}, {'_id': 0})

    existing_records_set = {tuple(record.values()) for record in existing_records}

    non_duplicate_data = []

    for jam in traffic_jams:
        # Convert the current data to a tuple for comparison
        current_data_tuple = tuple(jam.values())

        if current_data_tuple not in existing_records_set:
            # If the data is not a duplicate, append it to the list
            non_duplicate_data.append(jam)
    
    if non_duplicate_data:
        collection.insert_many(non_duplicate_data)
        client.close()
        return f"Received {len(traffic_jams)} records. {len(non_duplicate_data)} Traffic jams inserted successfully."
    else:
        client.close()
        return f"Received {len(traffic_jams)} records. No traffic jams to insert."
    
@app.route('/trafficjam', methods=['GET'])
def get_trafficjam():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['jvs_sample_trafficjam']

    cursor = collection.find()

    traffic_jams = []

    for document in cursor:
        document["_id"] = str(document["_id"])
        traffic_jams.append(document)

    client.close()

    return jsonify(traffic_jams)

@app.route('/clean_trafficjam', methods=['GET'])
def clean_trafficjam():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['jvs_sample_trafficjam']

    max_documents = 100

    current_count = collection.count_documents({})

    if current_count > max_documents:
        # Calculate the number of documents to remove
        documents_to_remove = current_count - max_documents
        
        # Get the oldest documents
        oldest_documents = collection.find().sort("_id", 1).limit(documents_to_remove)
        
        # Create a list of ObjectIds for the documents to remove
        documents_to_remove_ids = [doc["_id"] for doc in oldest_documents]
        
        # Remove the oldest documents
        try:
            result = collection.delete_many({"_id": {"$in": documents_to_remove_ids}})
            return f"Deleted {result.deleted_count} documents."
        except Exception as bwe:
            return f"Error deleting documents: {bwe.details}"
    else:
        return "No action needed. Document count is within the limit."

@app.route('/save_roadclosure', methods=['GET'])
def save_roadclosure():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['jvs_sample_roadclosure']

    headers = { 'AccountKey' : apikey,
             'accept' : 'application/json'}

    response = requests.get('http://datamall2.mytransport.sg/ltaodataservice/TrafficIncidents', headers=headers)
    data = response.json().get("value", [])  # Use .get() method to get the value or return an empty list if it doesn't exist

    if not data:
        client.close()
        return "No traffic incidents data available from the API."
    
    df = pd.DataFrame(data)
    df[['Date', 'Time']] = df['Message'].str.extract(r'\((.*?)\)(.*?) ')

    df['Message'] = df['Message'].str.replace(r'\(\d+/\d+\)\d+:\d+ ', '', regex=True)

    current_year = pd.Timestamp.now().year
    df['Date'] = df['Date'] + '/' + str(current_year)

    closures = df.loc[df['Type'] == "Road Block"]

    road_closures = []

    geolocator = Nominatim(user_agent="myGeocoder")

    for index, row in closures.iterrows():
        date = row["Date"]
        time = row["Time"]
        message = row['Message']
        location = str(row["Latitude"]) + "," + str(row["Longitude"])
        try:
            location_info = geolocator.reverse(location, exactly_one=True)
            address = location_info.address
        except Exception as e:
            address = None
        
        if address is not None:
            road_closure = {}

            road_closure["date"] = date
            road_closure["time"] = time
            road_closure["message"] = message
            road_closure["location"] = location
            road_closure["address"] = address

            road_closures.append(road_closure)   

    existing_records = collection.find({}, {'_id': 0})

    existing_records_set = {tuple(record.values()) for record in existing_records}

    non_duplicate_data = []

    for closure in road_closures:
        # Convert the current data to a tuple for comparison
        current_data_tuple = tuple(closure.values())

        if current_data_tuple not in existing_records_set:
            # If the data is not a duplicate, append it to the list
            non_duplicate_data.append(closure)
    
    if non_duplicate_data:
        collection.insert_many(non_duplicate_data)
        client.close()
        return f"Received {len(road_closures)} records. {len(non_duplicate_data)} Road closures inserted successfully."
    else:
        client.close()
        return f"Received {len(road_closures)} records. No traffic jams to insert."
    
@app.route('/roadclosure', methods=['GET'])
def get_roadclosure():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['jvs_sample_roadclosure']

    cursor = collection.find()

    road_closures = []

    for document in cursor:
        document["_id"] = str(document["_id"])
        road_closures.append(document)

    client.close()

    return jsonify(road_closures)

@app.route('/clean_roadclosure', methods=['GET'])
def clean_roadclosure():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['jvs_sample_roadclosure']

    max_documents = 100

    current_count = collection.count_documents({})

    if current_count > max_documents:
        # Calculate the number of documents to remove
        documents_to_remove = current_count - max_documents
        
        # Get the oldest documents
        oldest_documents = collection.find().sort("_id", 1).limit(documents_to_remove)
        
        # Create a list of ObjectIds for the documents to remove
        documents_to_remove_ids = [doc["_id"] for doc in oldest_documents]
        
        # Remove the oldest documents
        try:
            result = collection.delete_many({"_id": {"$in": documents_to_remove_ids}})
            return f"Deleted {result.deleted_count} documents."
        except Exception as bwe:
            return f"Error deleting documents: {bwe.details}"
    else:
        return "No action needed. Document count is within the limit."

@app.route('/save_roadaccident', methods=['GET'])
def save_roadaccident():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['jvs_sample_roadaccident']

    headers = { 'AccountKey' : apikey,
             'accept' : 'application/json'}

    response = requests.get('http://datamall2.mytransport.sg/ltaodataservice/TrafficIncidents', headers=headers)
    data = response.json().get("value", [])  # Use .get() method to get the value or return an empty list if it doesn't exist

    if not data:
        client.close()
        return "No traffic incidents data available from the API."
    
    df = pd.DataFrame(data)
    df[['Date', 'Time']] = df['Message'].str.extract(r'\((.*?)\)(.*?) ')

    df['Message'] = df['Message'].str.replace(r'\(\d+/\d+\)\d+:\d+ ', '', regex=True)

    current_year = pd.Timestamp.now().year
    df['Date'] = df['Date'] + '/' + str(current_year)

    accidents = df.loc[df['Type'] == "Accident"]

    road_accidents = []

    geolocator = Nominatim(user_agent="myGeocoder")

    for index, row in accidents.iterrows():
        date = row["Date"]
        time = row["Time"]
        message = row['Message']
        location = str(row["Latitude"]) + "," + str(row["Longitude"])
        try:
            location_info = geolocator.reverse(location, exactly_one=True)
            address = location_info.address
        except Exception as e:
            address = None

        if address is not None:
            road_accident = {}

            road_accident["date"] = date
            road_accident["time"] = time
            road_accident["message"] = message
            road_accident["location"] = location
            road_accident["address"] = address

            road_accidents.append(road_accident)   

    existing_records = collection.find({}, {'_id': 0})

    existing_records_set = {tuple(record.values()) for record in existing_records}

    non_duplicate_data = []

    for accident in road_accidents:
        # Convert the current data to a tuple for comparison
        current_data_tuple = tuple(accident.values())

        if current_data_tuple not in existing_records_set:
            # If the data is not a duplicate, append it to the list
            non_duplicate_data.append(accident)
    
    if non_duplicate_data:
        collection.insert_many(non_duplicate_data)
        client.close()
        return f"Received {len(road_accidents)} records. {len(non_duplicate_data)} Road accidents inserted successfully."
    else:
        client.close()
        return f"Received {len(road_accidents)} records. No road accidents to insert."
    
@app.route('/roadaccident', methods=['GET'])
def get_roadaccident():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['jvs_sample_roadaccident']

    cursor = collection.find()

    road_accidents = []

    for document in cursor:
        document["_id"] = str(document["_id"])
        road_accidents.append(document)

    client.close()

    return jsonify(road_accidents)

@app.route('/clean_roadaccident', methods=['GET'])
def clean_roadaccident():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['jvs_sample_roadaccident']

    max_documents = 100

    current_count = collection.count_documents({})

    if current_count > max_documents:
        # Calculate the number of documents to remove
        documents_to_remove = current_count - max_documents
        
        # Get the oldest documents
        oldest_documents = collection.find().sort("_id", 1).limit(documents_to_remove)
        
        # Create a list of ObjectIds for the documents to remove
        documents_to_remove_ids = [doc["_id"] for doc in oldest_documents]
        
        # Remove the oldest documents
        try:
            result = collection.delete_many({"_id": {"$in": documents_to_remove_ids}})
            return f"Deleted {result.deleted_count} documents."
        except Exception as bwe:
            return f"Error deleting documents: {bwe.details}"
    else:
        return "No action needed. Document count is within the limit."
    
@app.route('/save_erp', methods=['GET'])
def save_erp():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['erp_rates']

    headers = { 'AccountKey' : apikey,
             'accept' : 'application/json'}

    response = requests.get("http://datamall2.mytransport.sg/ltaodataservice/ERPRates", headers=headers)
    data = response.json().get("value", [])  # Use .get() method to get the value or return an empty list if it doesn't exist

    if not data:
        client.close()
        return "No ERP rates data available from the API."
    
    collection.delete_many({})

    result = collection.insert_many(data)

    client.close()

    return f"Inserted {len(result.inserted_ids)} new ERP rates."
    
@app.route('/erp', methods=['GET'])
def get_erp():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['erp_rates']

    cursor = collection.find()

    erp_rates = []

    for document in cursor:
        document["_id"] = str(document["_id"])
        erp_rates.append(document)

    client.close()

    return jsonify(erp_rates)

@app.route('/erp_time', methods=['GET'])
def get_erp_time():

    client = pymongo.MongoClient(mongo_uri)

    db = client['TraffoozeDBS']
    collection = db['erp_rates']

    cursor = collection.find()

    time_ranges = set()

    for document in cursor:
        start_time = document['StartTime']
        end_time = document['EndTime']
        time_range = f"{start_time} to {end_time}"
        time_ranges.add(time_range)

    sorted_time_ranges = sorted(time_ranges, key=lambda x: (datetime.strptime(x.split(' to ')[0], '%H:%M'), datetime.strptime(x.split(' to ')[1], '%H:%M')))

    client.close()

    return jsonify(sorted_time_ranges)

@app.route('/get_locations', methods=['GET'])
def get_locations_metadata():

    mongo_url = mongo_uri
    client = pymongo.MongoClient(mongo_url)
    db = client['TraffoozeDBS']
    collection = db['roads_metadata']

    projection = {
        "_id": 0,
        "road_id": 0,
        "length": 0,
        "shape": 0,
        "coord_list": 0,
    }

    roads_metadata = list(collection.find(projection=projection))

    client.close()

    formatted_metadata = []
    for road in roads_metadata:
        location = f"{road['start_lat']},{road['start_lng']}"
        formatted_road = {
            "address": road["description"],
            "location": location
        }
        formatted_metadata.append(formatted_road)

    if formatted_metadata:
        return jsonify(formatted_metadata)
    else:
        return jsonify(message="No data found"), 404

@app.route('/traffic_icons', methods=['GET'])
def get_traffic_icons():

    client = pymongo.MongoClient(mongo_uri)
    db = client['TraffoozeDBS']

    traffic_updates = []

    for collection_name in ["jvs_sample_trafficjam", "jvs_sample_roadclosure", "jvs_sample_roadaccident"]:
        collection = db[collection_name]

        if collection_name in ["jvs_sample_trafficjam", "jvs_sample_roadaccident"]:
            current_datetime_utc = datetime.utcnow()
            utc_offset = timedelta(hours=8)
            current_datetime = current_datetime_utc + utc_offset
            current_date = current_datetime.date()

            formatted_date = f"{current_date.day}/{current_date.month}/{current_date.year}"

            updates = collection.find({"date": formatted_date})
        else:
            updates = collection.find()

        formatted_updates = [{
            "address": update["address"],
            "date": update["date"],
            "time": update["time"],
            "message": update["message"],
            "location": update["location"],
            "type": collection_name
        } for update in updates]
        traffic_updates.extend(formatted_updates)

    return jsonify(traffic_updates)

@app.route('/traffic_updates', methods=['GET'])
def get_traffic_updates():

    client = pymongo.MongoClient(mongo_uri)
    db = client['TraffoozeDBS']

    traffic_updates = []

    for collection_name in ["jvs_sample_trafficjam", "jvs_sample_roadclosure", "jvs_sample_roadaccident"]:
        collection = db[collection_name]

        updates = collection.find()

        formatted_updates = [{
            "address": update["address"],
            "date": update["date"],
            "time": update["time"],
            "message": update["message"],
            "location": update["location"],
        } for update in updates]
        traffic_updates.extend(formatted_updates)

    return jsonify(traffic_updates)

scheduler = BackgroundScheduler(daemon=True)
scheduler.add_job(save_trafficjam, 'interval', minutes=5)
scheduler.add_job(save_roadclosure, 'interval', minutes=5)
scheduler.add_job(save_roadaccident, 'interval', minutes=5)
scheduler.add_job(clean_trafficjam, 'interval', minutes=15)
scheduler.add_job(clean_roadclosure, 'interval', minutes=15)
scheduler.add_job(clean_roadaccident, 'interval', minutes=15)
scheduler.add_job(save_erp, 'interval', days=1)
scheduler.start()

if __name__ == '__main__':
    celery.start()
    app.run(port=3000, debug=True)

#yep