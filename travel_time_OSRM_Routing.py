import os
import requests
import polyline
import math
import json
import pandas as pd
import pymongo
from google.cloud import bigquery

def lambda_handler(event, context):

    data = json.loads(event["body"])
    
    timestamp = data["timestamp"]
    coordinates = data["coordinates"]

    service_account_info = {
        "type": os.environ.get("type"),
        "project_id": os.environ.get("project_id"),
        "private_key_id": os.environ.get("private_key_id"),
        "private_key": os.environ.get("private_key"),
        "client_email": os.environ.get("client_email"),
        "client_id": os.environ.get("client_id"),
        "auth_uri": os.environ.get("auth_uri"),
        "token_uri": os.environ.get("token_uri"),
        "auth_provider_x509_cert_url": os.environ.get("auth_provider_x509_cert_url"),
        "client_x509_cert_url": os.environ.get("client_x509_cert_url"),
        "universe_domain": os.environ.get("universe_domain")
    }

    client = bigquery.Client.from_service_account_info(service_account_info)

    query = f"""
        SELECT road_id, jamFactor_prediction
        FROM `disco-math-385807.traffooze.traffic_flow_predictions`
        WHERE timestamp = DATETIME('{timestamp}')
    """

    query_job = client.query(query)

    results = query_job.result()

    pred_list = []

    for row in results:
        traffic_object = {}

        traffic_object["road_id"] = row[0]
        traffic_object["jamFactor_prediction"] = row[1]

        pred_list.append(traffic_object)

    pred_df = pd.DataFrame(pred_list)

    mongo_uri = os.environ.get('MONGO_URI')

    mongo_url = mongo_uri
    client = pymongo.MongoClient(mongo_url)
    db = client['TraffoozeDBS']
    collection = db['roads_metadata']

    projection = {"_id": 0, "road_id": 1, "coord_list":1}

    cursor = collection.find(projection=projection)

    data_list = list(cursor)
    metadata_df = pd.DataFrame(data_list)

    client.close()

    pred_df = pred_df.merge(metadata_df, on='road_id', how='left')

    osrm_url = "http://router.project-osrm.org/route/v1/driving/{coordinates}"

    osrm_params = {
        "steps": "true",
        "overview": "full",
        "alternatives": "3"
    }

    osrm_headers = {
        "User-Agent": "Mozilla/5.0"
    }

    osrm_response = requests.get(osrm_url.format(coordinates=coordinates), params=osrm_params, headers=osrm_headers)
    osrm_data = osrm_response.json()

    osrm_routes = osrm_data["routes"]

    route_list = []

    for osrm_route in osrm_routes:
        osrm_legs = osrm_route["legs"]
        total_distance = 0
        total_duration = 0
        total_predicted_duration = 0
        total_jam_factor = 0
        route_geometry = osrm_route["geometry"]  # Extract the geometry

        decoded_geometry = polyline.decode(route_geometry)  # Decode the geometry

        for osrm_leg in osrm_legs:
            osrm_steps = osrm_leg["steps"]

            for osrm_step in osrm_steps:
                step_coordinates = polyline.decode(osrm_step["geometry"])

                closest_route_index = find_closest_route(step_coordinates, pred_df)
                closest_jam_factor = pred_df.loc[closest_route_index, 'jamFactor_prediction']
                total_jam_factor += closest_jam_factor

                step_duration = osrm_step["duration"]
                predicted_duration = step_duration * (1 + closest_jam_factor / 10)

                total_distance += osrm_step["distance"]
                total_duration += step_duration
                total_predicted_duration += predicted_duration

        total_jam_factor_formatted = "{:.2f}%".format(total_jam_factor * 10)

        route_summary = {
            "total_distance": total_distance,
            "total_duration": total_duration,
            "total_predicted_duration": total_predicted_duration,
            "total_jam_factor": total_jam_factor_formatted,
            "decoded_geometry": decoded_geometry  # Include decoded geometry
        }

        route_list.append(route_summary)

    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Origin': '*',  # Allow requests from all origins
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET',
        },
        'body': json.dumps(route_list)
    }

def haversine_distance(coord1, coord2):
    lat1, lon1 = coord1
    lat2, lon2 = coord2

    R = 6371.0  # Earth's radius in kilometers

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)

    a = math.sin(dlat / 2)**2 + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.sin(dlon / 2)**2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    distance = R * c
    return distance

def find_closest_route(target_route, routes_df):
    closest_distance = float('inf')
    closest_route_index = None

    for idx, route_coords in enumerate(routes_df['coord_list']):
        total_distance = sum(haversine_distance(coord1, coord2) for coord1, coord2 in zip(target_route, route_coords))
        
        if total_distance < closest_distance:
            closest_distance = total_distance
            closest_route_index = idx

    return closest_route_index